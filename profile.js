define([
	'require',
	'madjoh_modules/cache/data_cache',
	'madjoh_modules/ajax/ajax',
	'madjoh_modules/custom_events/custom_events',
	'madjoh_modules/session/session'
],
function(require, DataCache, AJAX, CustomEvents, Session){
	var Profile = {
		init : function(){
			CustomEvents.addCustomEventListener(document, 'LoggedIn', function(){Profile.get();});
		},
		get : function(callback, user_id, user_slug){
			if(!Session.isOpen()) return;
			if(user_id === Session.getKeys().id) user_id = null;

			var cacheKey = 'profile';
			if(user_id) cacheKey += '_' + user_id;
			if(user_slug) cacheKey += '_' + user_slug;
			var profile = DataCache.get(cacheKey, 1000 * 3600 * 24 * 365); // 1 Year Delay

			if(profile){
				if(callback) callback(profile);
				return;
			}
			var parameters = (user_id) ? {user_id : user_id} : {};
			if(user_slug) parameters.user_slug = user_slug;

			var route = '/user/get/profile';
			if(user_id === null && user_slug) route += '/slug';
			AJAX.post(route, {parameters : parameters}).then(function(result){
				var profile = result.data.profile;
				DataCache.set(cacheKey, profile, user_id ? true : false);
				if(callback) callback(profile);
			});
		},
		save : function(new_profile, callback){
			Profile.get(function(profile){
				for(var key in new_profile) profile[key] = new_profile[key];
				DataCache.set('profile', profile);
				if(callback) callback();
			});
		},

		updateCredits : function(coins, callback){
			Profile.get(function(profile){
				profile.coins = coins;
				DataCache.set('profile', profile);
				CustomEvents.fireCustomEvent(document, 'NewCredits');
				if(callback) callback();
			});
		},
		publicKeys : ['id', 'firstname', 'lastname', 'photo', 'facebook_id', 'certified'],
		parseToPublic : function(profile){
			var publicProfile = {};
			for(var i = 0; i < Profile.publicKeys.length; i++){
				publicProfile[Profile.publicKeys[i]] = profile[Profile.publicKeys[i]];
			}
			return publicProfile;
		}
	};

	return Profile;
});